class ScraperService
	require 'open-uri'

	class << self

		def get_doc_for(url)
			Nokogiri::HTML.parse(open(url))
		end

		def get_item_details(url)
			doc = get_doc_for(url)
			name = doc.at_css('.item_name').text()
			modifiers = doc.
				css('.item_modifier-options').
				collect{|x| x.at_css('label').children.first.text()}

			price = doc.at_css('.item_price-text').text()
			Item.new(name: name, price: price, modifiers: modifiers)
		end

		def scratch(domain, url)
			items = []
			if item_detail_doc(url)
				[items.push(get_item_details(url)), '']
			elsif merchant_detail_doc(url).present? && 
				browser = Watir::Browser.new(:firefox)
				begin
					browser.goto url
					html = browser.div(class: 'merchant-content-container').inner_html
					browser.close
					merchant_page = Nokogiri::HTML.parse(html)
					links = merchant_page.css('.offer-tile_link').collect{|x| x['href']}
					links.each do |url|
						items.push get_item_details('https://'+ domain + url)
					end
					[items, '']
				rescue Exception => error
					[[], error.message]
				end
			else
				[[], 'Sorry, Not able to serve your request']
			end
		end

		def item_detail_doc(url)
			begin
				doc = get_doc_for(url)
				!doc.xpath('//div[starts-with(@class, "item_content")]').empty?
			rescue
				return false
			end
		end

		def merchant_detail_doc(url)
			begin
				doc = get_doc_for(url)
				!doc.xpath('//body[starts-with(@class, "merchant-page")]').empty?
			rescue
				return false
			end
		end
	end
end
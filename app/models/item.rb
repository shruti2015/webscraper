class Item
	include ActiveModel::Model
	attr_accessor :name, :modifiers, :price

	def initialize(attributes = {})
		attributes.each do |name, value|
			send("#{name}=", value)
		end
	end
end
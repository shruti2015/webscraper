class ScrapersController < ApplicationController

  def index
    @items = []
  end

  def scrape
    domain_name = URI.parse(params[:url]).host
    if domain_name == 'www.trycaviar.com'
      @items, @message = ScraperService.scratch(domain_name, params[:url])
    else
      @message = params[:url].blank? ?
        'Please, Fill the url' :
        'Sorry, not serving domain other than www.trycaviar.com'

      render 'scrape_error'
    end
  end

end
